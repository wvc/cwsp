# Crane-Operated Warehouse Scheduling Problem - data instances, solutions & validator

This Bitbucket-repository serves as a supplemental data repository to the original research article: 'Crane-operated warehouses: Integrating location assignment and crane scheduling' submitted to Computers & Industrial Engineering, by the the following authors:

- Sam Heshmati
	- KU Leuven, Dept. of Computer Science, CODeS - Belgium
	- University of Porto, Faculty of Engineering, INESC TEC - Portugal
- T�lio A. M. Toffolo, Prof.
	- Federal University of Ouro Preto, Department of Computing - Brazil
- Wim Vancroonenburg, PhD. (**corresponding author** - e-mail: wim.vancroonenburg@cs.kuleuven.be)
	- KU Leuven, Dept. of Computer Science, CODeS - Belgium
	- Research Foundation Flanders (FWO Vlaanderen) - Belgium
- Greet Vanden Berghe, Prof.
	- KU Leuven, Dept. of Computer Science, CODeS - Belgium
	
	
The repository contains three main data/code sections: data instances, solutions, validator

---
## Data instances

The 'Instances' folder contains all the data instances used in the computational section of the article. Each individual instance is specified as a JSON formatted file with the following structure:
```json
{
	"parameters": {
		"number_of_cranes": "2",
        "crane_safety_time": "3",
        "stacking_level": "1"
	},
	"blocks": [
		{
			"id": "0",
            "type": "yard",
            "xcor": "1",
            "ycor": "1",
            "zcor": "0",
            "neighbour_blocks": [
                "1",
                "50"
            ],
            "times_to_blocks": [
				"0",
				"1",
				...
			]
		},
		...
		{
			"id": "1000",
            "type": "io",
            "xcor": "0",
            "ycor": "0",
            "times_to_blocks": [
		},
		...
	],
	"products": [
		{
			"id": "0",
            "type": "yard",
            "block_id": "930"
		},
		...
		{
			"id": "141",
            "type": "output",
            "source_block_id": "507",
            "dest_block_id": "1109",
            "rel_time": "21.299999237060547",
            "due_time": "32.514549761451569"
		},
		...
		{
			"id": "308",
            "type": "input",
            "source_block_id": "1134",
            "rel_time": "28",
            "due_time": "42.208789072410482",
            "costs_proximity_products": [
			]
		},
		...
	]
}
```

As can be seen, the instance is composed of three main elements: parameters, blocks  and products. The parameters are the number of cranes in the instance, the time required for cranes to move over the safety distance (considered to be equal over all adjacent pairs of cranes), and the number of stacking levels in the storagea area (1 = only storing on ground level).

Blocks correspond to 'locations' in the paper, and can be of two types: 'yard' and 'io' . Blocks are defined by their id, type, coordinates and specify their neighbouring blocks and their crane travel time to all other blocks.
'io' blocks are only on ground level and do no specify a z-coordinate and are not relevant for calculating proximity costs.

Products can be of three types: 'yard', 'input' and 'output'. 'yard' products are products which are currently stored in the yard and specify their current location (block_id). 'input' products specify their current source_block from which they must be moved. They also specify the moment when they are available to be moved and the due time before which they must be moved. 'output' products additionaly specify their destination block.

---
## Solutions

The 'Solutions' folder contains all the solutions obtained by the MILP formulations (solved with Gurobi) and the heuristic algorithm.

---
## Validator

The 'Validator' folder contains the C++ source code required to build the validator, which allows to verify the validity of the solutions files (from Section 'Solutions'). We provide the source code in order to enable scrutinizing the correctness of the validator.
