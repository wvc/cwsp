#include "solution.h"
#include "graph.h"
#include <boost/format.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>

using boost::property_tree::ptree;

double solution::craneStartTime(request* p) {
    crane* otherCrane = &Cranes[p->otherCraneId()];

    auto movesSize = (int) otherCrane->moves.size();

    for (int i = movesSize - 1; i >= 0; i -= 1) {
        request * m = otherCrane->moves[i];
        if (conflictMoves(m, p)) {
            if (p->craneId > otherCrane->id) {
                return std::max((m->endTime + m->destination->xcor - p->source->xcor + Problem->CST), std::max(m->startTime, p->product_ptr->releaseTime));
            }
            else {
                return std::max((m->endTime  + p->source->xcor - m->destination->xcor + Problem->CST), std::max(m->startTime, p->product_ptr->releaseTime));
            }
        }
    }
    return p->product_ptr->releaseTime;
}

void solution::setStartTime(request *p){
    request * lastSameCrane = previousMove(p);
    request * lastOtherCrane = previousMoveOtherCrane(p);

    double craneReadyTime, start_time, gantry_time;

    if (lastOtherCrane == nullptr) {
        if (lastSameCrane == nullptr) {
            start_time = p->product_ptr->releaseTime;
        }
        else {
            craneReadyTime = lastSameCrane->endTime + lastSameCrane->destination->BtoB[p->source->id];
            start_time = std::max(craneReadyTime, p->product_ptr->releaseTime);
        }
    }
    else {
        if (lastSameCrane == nullptr) {
            start_time = craneStartTime(p);
        }
        else {
            gantry_time = gantryTime(p);
            craneReadyTime = lastSameCrane->endTime + lastSameCrane->destination->BtoB[p->source->id];
            start_time = std::max(p->product_ptr->releaseTime, std::max(gantry_time, craneReadyTime));
        }
    }

    double end_time = start_time + p->duration();

    p->setTimes(start_time, end_time);
}

bool solution::conflictMoves(request *p, request *q) {
    if ((p->left() - Problem->CST < q->right()) &&
        (q->left() - Problem->CST < p->right()))
        return true;
    if ((p->right() < q->left()) && (q->craneId < p->craneId))
        return true;
    return (q->right() < p->left()) && (p->craneId < q->craneId);
}

double solution::gantryTime( request *p){
    double gantry_time = 0;
    crane *otherCrane =  &Cranes[p->otherCraneId()];
    request * lastSameCrane = previousMove(p);
    double craneReadyTime;

    for (int i = (int) otherCrane->moves.size() - 1; i >= 0; i--) {
        request *m = otherCrane->moves[i];
        if (conflictMoves(m, p)){
            if (p->craneId > otherCrane->id){
                if (m->destination->xcor > p->source->xcor){
                    if(p->source->xcor < p->destination->xcor &&  m->source->xcor < p->source->xcor){
                        craneReadyTime = lastSameCrane->endTime + lastSameCrane->destination->BtoB[p->source->id];
                        if(craneReadyTime < lastSameCrane->startTime + ( p->source->xcor - m->source->xcor - Problem->CST)){
                            return craneReadyTime;
                        }
                        else{
                            gantry_time = m->endTime + m->destination->xcor - p->source->xcor;
                            return std::max(gantry_time + Problem->CST, m->startTime);
                        }
                    }
                    else{
                        gantry_time = m->endTime + m->destination->xcor - p->source->xcor;
                        return std::max(gantry_time + Problem->CST, m->startTime);
                    }

                }
                else {
                    if (m->destination->xcor > m->source->xcor){
                        gantry_time = m->endTime + m->destination->xcor - p->source->xcor;
                        return std::max(gantry_time + Problem->CST, m->startTime);
                    }
                    else {
                        gantry_time = m->source->xcor - p->source->xcor;
                        return std::max(m->startTime + gantry_time + Problem->CST, m->startTime);
                    }
                }
            }
            else {
                if (m->destination->xcor < p->source->xcor){
                    if(p->source->xcor > p->destination->xcor &&  m->source->xcor > p->source->xcor) {
                        craneReadyTime = lastSameCrane->endTime + lastSameCrane->destination->BtoB[p->source->id];
                        if(craneReadyTime < lastSameCrane->startTime + (m->source->xcor - p->source->xcor - Problem->CST)){
                            return craneReadyTime;
                        }
                        else{
                            gantry_time = m->endTime + p->source->xcor - m->destination->xcor;
                            return std::max(gantry_time + Problem->CST, m->startTime);
                        }
                    }
                    else{
                        gantry_time = m->endTime + p->source->xcor - m->destination->xcor;
                        return std::max(gantry_time + Problem->CST, m->startTime);
                    }
                }
                else {
                    if (m->source->xcor > m->destination->xcor){
                        gantry_time = m->endTime + p->source->xcor - m->destination->xcor;
                        return std::max(gantry_time + Problem->CST, m->startTime);
                    }
                    else {
                        gantry_time = p->source->xcor - m->source->xcor;
                        return std::max(m->startTime + gantry_time + Problem->CST, m->startTime);
                    }
                }
            }
        }
    }

    return gantry_time;
}

double solution::evaluate() {
    double totalCost = 0;
    double delayCost = 0;
    int PonPCost = 0;
    setStartTimes();
    for (auto &m :Moves) {
        if (m.type != "yard_movement")
            delayCost += m.delay;
        if (m.type != "output") {
            for (auto &nb: m.destination->neighbors) {
                if (invPlaces[nb->id] != nullptr)
                    PonPCost += m.product_ptr->PonPCosts[invPlaces[nb->id]->id];
            }
        }
    }
    totalCost = (Problem->beta * delayCost) + (Problem->alpha * PonPCost);
    this->delayCost = delayCost;
    this->storageCost = PonPCost;
    this->objective = totalCost;
    return totalCost;
}

bool solution::schedulingConstraints() {

    for(auto& move : Moves){
        ///assert that if an input request is assigned atop another input request, the bottom request is placed first.
        if (move.type != "output"){
            location* destination = Places[move.product_ptr->id];
            if (destination->aboveBlock != nullptr)
                if (invPlaces [destination->aboveBlock->id] != nullptr)
                    if (pMoves [invPlaces [destination->aboveBlock->id]->id]->startTime < move.endTime)
                        return false;
        }

        ///ensure j is moved after i if it has been assigned to i's origin location.
        if (move.type != "input"){
            location* origin = move.source;
            if (invPlaces [origin->id] != nullptr)
                if(pMoves[invPlaces[origin->id]->id]->endTime < move.startTime)
                    return false;
        }

        ///``move" may only begin after all its preceding requests are finished.
        if (move.product_ptr->precedence.empty())
            continue;

        for (auto &pr : move.product_ptr->precedence){
            if (pMoves[pr]->endTime > move.startTime)
                return false;
        }
    }
    return true;
}

request *solution::previousMove(request *p) {
    int crane = p->craneId;
    auto schedule_size = (int) Cranes[crane].moves.size();
    if (schedule_size < 2)
        return nullptr;

    return Cranes[crane].moves[schedule_size - 2];
}

request *solution::previousMoveOtherCrane(request *p) {
    int other = std::abs(p->craneId - 1);
    auto schedule_size = (int) Cranes[other].moves.size();
    if (schedule_size == 0)
        return nullptr;

    return Cranes[other].moves[schedule_size - 1];
}

void solution::setStartTimes() {
    for (auto &c :Cranes)
        c.moves.clear();

    for(auto &m : Schedule) {
        Cranes[m->craneId].moves.push_back(m);
        setStartTime(m);
    }
}

void solution::fromFile(std::string str) {
    std::stringstream ss;
    ss << str;

    ptree pt;
    boost::property_tree::read_json(str, pt);
    std::vector < std::pair<double , request * > > requests;

    for (auto &p : pt.get_child("products")) {

        int productId = p.second.get<int>("id");
        std::string type = p.second.get<std::string>("type");
        int blockId = p.second.get<int>("destination_block_id");
        int crane = p.second.get<int>("crane");
        double startTime = p.second.get<double>("start_time");

        request * m = pMoves[productId];
        if (type == "input") {
            location *b = Problem->yardBlocks[blockId];
            Places[productId] = b;
            invPlaces[b->id] = m->product_ptr;
            m->destination = b;
        }
        m->craneId = crane;


        requests.push_back(std::make_pair(startTime, m));
    }

    std::sort(requests.begin(), requests.end());
    for(auto p : requests)
        Schedule.push_back(p.second);
}

bool solution::validate() {
    bool validation = true;
    for (auto &m : Moves ){
        if (m.type != "output"){
            int product_id = m.product_ptr->id;

            if (Places[product_id] == nullptr) {
                std::cout << "Product %d is assigned to no location \n" << product_id;
                return false;
            }
            else {
                if (invPlaces[Places[product_id]->id] == nullptr){
                    std::printf("The destination of product %d is empty \n", product_id);
                    return false;
                }
                else if (invPlaces[Places[product_id]->id]->id != product_id) {
                    int product = invPlaces[Places[product_id]->id]->id;
                    std::printf("Product %d and Block %d are inconsistent. Product: %d \n", product_id, Places[product_id]->id, product);
                    return false;
                }
            }

            if (m.product_ptr->source_block->id == Places[product_id]->id) {
                std::printf("Product %d is assigned to its source location\n", product_id);
                return false;
            }

            if (Places[product_id] != nullptr)  {
                location * destination = Places[product_id];
                for (auto &b : destination->underneathBlocks){
                    if (invPlaces[b->id] == nullptr) {
                        std::cout << "Product %d is stacked on top of empty location %d \n" << product_id << " " << b->id;
                        return false;
                    }
                }
            }
        }
    }

    validation = schedulingConstraints();

    if (validation){
        evaluate();
        printf( "The solution is feasible, storage cost: %d, scheduling cost: %0.3f, total cost: %0.3f\n", storageCost, delayCost, objective);
    }


    return validation;
}
