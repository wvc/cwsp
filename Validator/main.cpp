#include <iostream>
#include "solution.h"

int main(int argc, char* argv[]) {

    int index = 0;
    std::string input_file, solution_file;
    double alpha = 0.5, beta = 0.5;

    for (index = 1; index < argc; index += 2) {
        if (std::string(argv[index]) == "-p")
            input_file =  (argv[index + 1]);
        else if (std::string(argv[index]) == "-s")
            solution_file = (argv[index + 1]);
        else if (std::string(argv[index]) == "-a")
            alpha = std::stod(argv[index + 1]);
        else if (std::string(argv[index]) == "-b")
            beta = std::stod(argv[index + 1]);
        else
            std::cout << "Non-recognized command : " << (argv[index]) << std::endl;
    }


    problem p(input_file, alpha, beta);
    solution s(&p);
    s.fromFile(solution_file);
    s.validate();

    return 0;
}