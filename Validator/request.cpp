#include "request.h"

request::request()
{
}

request::~request()
{
}

int request::right() {
 return std::max(source->xcor, destination->xcor);
}

int request::left() {
 return std::min(source->xcor, destination->xcor);
}

double request::duration() {
 return source->BtoB[destination->id];
}

int request::otherCraneId() {
 return std::abs(craneId - 1);
}

void request::setTimes(double start, double end) {
 startTime = start;
 endTime = end;
 delay = std::max((double)0, (endTime - product_ptr->dueTime));

}
