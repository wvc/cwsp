#ifndef BLOCK_H_
#define BLOCK_H_
#include <string>
#include <vector>
#include "product.h"

class location
{
public:
	location();
	~location();

	std::string type;
	std::vector<location*> neighbors;
	std::vector<int> BtoB;
	std::vector<location*> underneathBlocks;
	location* aboveBlock;
	product* product_ptr;
	int id;
	int xcor;
	int	ycor;
	int zcor;
	
	
};
#endif