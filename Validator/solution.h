#ifndef SOLUTION_H_
#define SOLUTION_H_

#include <vector>
#include <string>
#include <map>
#include "problem.h"
#include "request.h"
#include "graph.h"
#include <iostream>
#include <boost/format.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <random>

class solution {

public:
    solution() = default;

    explicit solution(problem *p) {
        this->Problem = p;
        this->Moves.reserve(p->ioProducts.size());
        this->endTimes.resize(p->ioProducts.size());
        this->startTimes.resize(p->ioProducts.size());
        this->Places.resize(p->Products.size());
        this->pMoves.resize(p->Products.size());
        this->invPlaces.resize(p->yardBlocks.size());

        for (auto &b : p->yardBlocks)
        {
            if (b->product_ptr == nullptr)
            {
                availableBlocks.push_back(b);
            }
            else
            {
                if(b->product_ptr->type != "yard")
                    availableBlocks.push_back(b);
                else {
                    invPlaces[b->id] = b->product_ptr;
                    Places[b->product_ptr->id] = b;
                }

            }

        }

        std::map<int, request *> move_map;
        graph G(Problem);
        G.topologicalSort();
        for (auto &pr : G.sortedProducts) {
            request m;
            m.id = (int) Moves.size();
            m.product_ptr = pr;
            m.source = pr->source_block;
            m.type = pr->type;
            if (m.type == "output") {
                m.need_la = false;
                m.destination = pr->destination_block;
            }
            else {
                m.need_la = true;
                m.destination = nullptr;
            }

            Moves.push_back(m);
            move_map[pr->id] = &Moves.back();
            pMoves[m.product_ptr->id] = &Moves.back();
        }

        for (auto &pr : Problem->ioProducts) {
            if (!pr->precedence.empty()) {
                for (auto &i:pr->precedence){
                    pMoves[pr->id]->precedences.push_back(move_map[i]);
                }
            }
        }

        for (int c = 0; c < Problem->craneNr; c++) {
            crane C;
            C.id = c;
            Cranes.push_back(C);
        }

        Cranes[0].currentBlock = Problem->yardBlocks[0];
        if (Problem->craneNr > 1)
            Cranes.back().currentBlock = Problem->yardBlocks.back();
    };
    ~solution() = default;

    double objective;
    double delayCost;
    int storageCost;
    problem *Problem;

    std::vector<request> Moves;
    std::vector<request*> Schedule;
    std::vector<crane> Cranes;
    std::vector<location *> Places;
    std::vector<product *> invPlaces;
    std::vector<request *> pMoves; //indexed based on product id containing the move pointers
    std::vector<double> endTimes;
    std::vector<double> startTimes;
    std::vector<location *> availableBlocks;

    bool conflictMoves(request *p, request *q);
    bool schedulingConstraints();
    bool validate();

    double craneStartTime(request *p);
    double evaluate();
    double gantryTime(request *p);

    void fromFile(std::string file);
    void setStartTime(request *p);
    void setStartTimes();

    request * previousMove (request *p);
    request * previousMoveOtherCrane(request *p);

};
#endif